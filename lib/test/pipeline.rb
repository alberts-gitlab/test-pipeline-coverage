require "test/pipeline/version"

module Test
  module Pipeline
    class Error < StandardError; end
    # Your code goes here...

    def self.foo
      'foo'
    end

    def self.bar
      'bar'
    end
  end
end
